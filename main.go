package main

import (
	"errors"
	"fmt"
)

type Tree struct {
	Levels int
	Nodes  []*Node
}

type Node struct {
	Value   string
	Level   int
	Numdown int
}

func NewTree(numlevels int) *Tree {
	t := Tree{Levels: numlevels}
	n := SumBefN(numlevels)
	nodes := make([]*Node, n)
	for i := 0; i < numlevels; i++ {
		for j := 0; j <= i; j++ {
			nodes[i*(i+1)/2+j] = &Node{Value: "X", Level: i + 1, Numdown: j + 1}
		}
	}
	t.Nodes = nodes
	return &t
}

func (t *Tree) Insert(level, numdown int, value string) error {
	node, err := t.Index(level, numdown)
	if err != nil {
		return err
	}
	node.Value = value
	return nil
}

func (t *Tree) Index(level, numdown int) (*Node, error) {
	if level > t.Levels {
		return nil, errors.New("Invalid index")
	}
	ind, ok := CoordToIndex(level, numdown)
	if ok {
		return t.Nodes[ind], nil
	}
	return nil, errors.New("Invalid index")
}

func main() {
	t := NewTree(5)
	t.Insert(4, 2, "Y")
	t.Insert(5, 3, "Z")
	node, err := t.Index(4, 2)
	if err == nil {
		fmt.Println(node.Value)
	} else {
		fmt.Println(err.Error())
	}
	node, err = t.Index(5, 3)
	if err == nil {
		fmt.Println(node.Value)
	} else {
		fmt.Println(err.Error())
	}
	node, err = t.Index(6, 4)
	if err == nil {
		fmt.Println(node.Value)
	} else {
		fmt.Println(err.Error())
	}
}

//SumBefN returns the sum of all numbers up to n
func SumBefN(n int) int {
	return n * (n + 1) / 2
}

//CoordToIndex translates level and numdown to index of array
func CoordToIndex(level, numdown int) (int, bool) {
	if numdown > level {
		return -1, false
	}
	ind := level*(level-1)/2 + numdown - 1
	if ind > SumBefN(level) {
		return -1, false
	}
	return ind, true
}
