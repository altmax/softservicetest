<?php
class Tree{
    private $levels;
    private $nodes;

    function Tree($numlevels) {
        $this->levels = $numlevels;
        $this->nodes = [];
        for ($i = 0; $i < $numlevels; $i++) {
            for ($j = 0; $j <= $i; $j++) {
                $this->nodes[$i*($i+1)/2+$j] = new Node("X", $i+1, $j+1);
            }
        }
    }

    public function Index($level, $numdown) {
        if ($level > $this->levels) {
            return false;
        }

        $ind = CoordToIndex($level, $numdown);
        if ($ind === false) {
            return false;
        }
        return $this->nodes[$ind];
    }

    public function GetLevels() {
        return $this->levels;
    }

    public function Insert($value, $level, $numdown) {
        $node = $this->Index($level, $numdown);
        if ($node === false) {
            return false;
        }
        $node->SetValue($value);
        return true;
    }

}

class Node{
    private $value;
    private $level;
    private $numdown;

    function Node($value, $level, $numdown) {
        $this->value = $value;
        $this->level = $level;
        $this->numdown = $numdown;
    }

    public function GetValue() {
        return $this->value;
    }

    public function SetValue($value) {
        $this->value = $value;
        return true;
    }
}

function SumBefN($n) {
    return $n * ($n + 1) / 2;
}

function CoordToIndex($level, $numdown) {
    if ($numdown > $level) {
        return false;
    }
    $ind = $level * ( $level - 1 ) / 2 + $numdown -1;
    if ($ind > SumBefN($level)) {
        return false;
    }
    return $ind;
}

$t = new Tree(5);
echo "<pre>";
print_r($t);
echo "</pre>";
$t->Insert("Y", 4, 2);
$t->Insert("Z", 5, 3);
$node = $t->Index(4, 2);
if ($node !== false) {
    echo "Node(4, 2) = ".$node->GetValue();
} else {
    echo "Node(4, 2) not found.";
}
echo "<br/>";
$node = $t->Index(5, 3);
if ($node !== false) {
    echo "Node(5, 3) = ".$node->GetValue();
} else {
    echo "Node(5, 3) not found.";
}
echo "<br/>";
$node = $t->Index(6, 4);
if ($node !== false) {
    echo "Node(6, 4) = ".$node->GetValue();
} else {
    echo "Node(6, 4) not found.";
}